package sample.animation;

import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 * Describes an animation of a control element.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/23/2020
 */
public class Shake {
    private TranslateTransition transition;

    public Shake(Node node) {
        transition = new TranslateTransition(Duration.millis(70), node);
        transition.setFromX(-10);
        transition.setByX(10);
        transition.setCycleCount(3);
        transition.setAutoReverse(true);
    }

    public void playAnimation() {
        transition.playFromStart();
    }
}
