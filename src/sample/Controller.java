package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import sample.animation.Shake;

/**
 * Controls sample.fxml.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/23/2020
 */
public class Controller {
    @FXML
    private ObservableList<String> units = FXCollections.observableArrayList("Meters to Centimeters",
            "Centimeters to Meters",
            "Meters to Kilometers",
            "Kilometers to Meters",
            "Kilometers to Miles",
            "Miles to Kilometers",
            "Inches to Centimeters",
            "Centimeters to Inches",
            "Feet to Centimeters",
            "Centimeters to Feet");

    @FXML
    private ComboBox<String> unitsComboBox;

    @FXML
    private TextField inputField;

    @FXML
    private TextField resultField;

    @FXML
    void initialize() {
        resultField.setEditable(false);
        unitsComboBox.getItems().addAll(units);
        unitsComboBox.setValue(units.get(0));
    }

    @FXML
    void convert() {
        double value;

        try {
            value = Double.parseDouble(inputField.getText());
        }
        catch(NumberFormatException NaN) {
            Shake inputFieldShake = new Shake(inputField);
            inputFieldShake.playAnimation();
            return;
        }

        String comboBoxValue = unitsComboBox.getValue();

        if(comboBoxValue.equals(units.get(0))) resultField.setText(String.format("%.2f", value * 100));
        else if(comboBoxValue.equals(units.get(1))) resultField.setText(String.format("%.2f", value / 100));
        else if(comboBoxValue.equals(units.get(2))) resultField.setText(String.format("%.2f", value / 1000));
        else if(comboBoxValue.equals(units.get(3))) resultField.setText(String.format("%.2f", value * 1000));
        else if(comboBoxValue.equals(units.get(4))) resultField.setText(String.format("%.2f", value * 1.609));
        else if(comboBoxValue.equals(units.get(5))) resultField.setText(String.format("%.2f", value / 1.609));
        else if(comboBoxValue.equals(units.get(6))) resultField.setText(String.format("%.2f", value * 2.54));
        else if(comboBoxValue.equals(units.get(7))) resultField.setText(String.format("%.2f", value / 2.54));
        else if(comboBoxValue.equals(units.get(8))) resultField.setText(String.format("%.2f", value * 30.48));
        else if(comboBoxValue.equals(units.get(9))) resultField.setText(String.format("%.2f", value / 30.48));
    }

    @FXML
    void clear() {
        inputField.clear();
        resultField.clear();
    }
}
